from AnoML import *
def CNN1D_Model(DF, DPs, ModelDetails):
  import tensorflow as tf
  if (tf.__version__ != "2.1.1"):
    print ("This function requires tensorflow version 2.1.1, please install required version and retry")
    return False, False
  TrainX, TrainY = Convert2TimeSeries(DF, DPs, ModelDetails["TS"], False)
  early_stop = tf.keras.callbacks.EarlyStopping(
          monitor='loss', min_delta=1e-2, patience=3, verbose=0, mode='auto',
          baseline=None, restore_best_weights=True)
  model = tf.keras.Sequential()
  model.add(tf.keras.layers.Conv1D(filters=ModelDetails["filters"], kernel_size=ModelDetails["kernel_size"], padding='same', activation='relu', input_shape=(ModelDetails["TS"], 1)))
  model.add(tf.keras.layers.GlobalMaxPool1D())
  model.add(tf.keras.layers.Dense(units=1, activation='linear'))
  model.compile(optimizer='adam', loss='mean_squared_error')
  history= model.fit(TrainX, TrainY, batch_size=ModelDetails["batch_size"], epochs=ModelDetails["epochs"], callbacks=[early_stop])
  del tf
  return history, model
def RNN_Model(ModelDetails, DF, DPs, Force):
    from time import time
    if (ModelDetails["BS"]>1 and len(DPs)>1):
        print ("Multivariate data should have a batch size of 1 for this model, changing automatically")
        ModelDetails["BS"]=1
    TrainX, TrainY = Convert2TimeSeries(DF, DPs, ModelDetails["TS"], ModelDetails["AE"])
    if (NeedThisModel(ModelDetails, "TF") or Force):
        from keras import Sequential
        from tensorflow.keras.callbacks import EarlyStopping
        from keras.layers import LSTM, InputLayer, RepeatVector, Dropout, TimeDistributed, Dense
        RNN_Layer=LSTM
        Neurons=ModelDetails["Neurons"]
        Epochs=ModelDetails["Epochs"]
        BatchSize=ModelDetails["BS"]
        TIME_STEPS=ModelDetails["TS"]
        Variants=len(DPs)
        early_stop = EarlyStopping(monitor='loss', min_delta=1e-2, patience=3,
                                   verbose=0, mode='auto', baseline=None, restore_best_weights=True)
        StartTime=time()
        model = Sequential()
        #model.add(InputLayer(input_shape=(TrainX.shape[1], TrainX.shape[2]), name='Input_Layer_1'))
        model.add(RNN_Layer(Neurons, input_shape=(TrainX.shape[1], TrainX.shape[2]), name="rnn_layer_2"))
        model.add(Dropout(rate=0.2, name="dropout_layer_3"))
        model.add(RepeatVector(n=TrainX.shape[1], name="repeatvector_layer_4"))
        model.add(RNN_Layer(Neurons, return_sequences=True, name="rnn_layer_5"))
        model.add(Dropout(rate=0.2, name="dropout_layer_6"))
        model.add(TimeDistributed(Dense(units=TrainX.shape[2], name="timedistributed_layer_7")))
        model.add(Dense(units=TrainX.shape[2], name="dense_layer_8"))
        model.compile(loss='mae', optimizer='adam')
        model.build()
        model.summary()
        history = model.fit(x=TrainX, y=TrainY, epochs=Epochs,
                            batch_size=BatchSize, callbacks=[early_stop], verbose=1)
        EndTime = time()
        ModelDetails["FT"] = 1000*(EndTime-StartTime)
        print ("Time consumed in model training ", ModelDetails["FT"], " for data shape: ", TrainX.shape)
        ModelDetails["Model"] = model
        StoreModel(ModelDetails, Force)
        ModelDetails["History"] = history
        del Sequential, LSTM, InputLayer, RepeatVector, Dropout, TimeDistributed, Dense, EarlyStopping
    else:
        import tensorflow as tf
        StartTime = time()
        model = tf.keras.models.load_model(ModelDetails["Location"]+"/"+ModelDetails["ModelName"])
        EndTime = time()
        ModelDetails["LT"]=1000*(EndTime - StartTime)
        ModelDetails["History"] = None
        #print (ModelDetails["FT"])
        del tf
    StartTime = time()
    PredictedValues = model.predict(TrainX)
    EndTime = time()
    import numpy as np
    Loss=np.mean(np.abs(PredictedValues - TrainX), axis=1)
    Loss.shape
    if (TrainX.shape[2]>1):
        Loss = np.mean(Loss)
        ModelDetails["Loss"] = Loss
    else:
        Loss= max(Loss)
        ModelDetails["Loss"] = Loss[0]
    ModelDetails["PT"] = 1000*(EndTime-StartTime)
    del np
    del TrainX, TrainY
    del time
    return ModelDetails

def OCSVM_Model(ModelDetails, DF, DPs, Force):
    from sklearn.svm import OneClassSVM
    from time import time
    if (NeedThisModel(ModelDetails, "SK") or Force):
        ST = time()
        model = OneClassSVM(nu=ModelDetails["nu"], kernel=ModelDetails["kernel"], gamma=ModelDetails["gamma"])
        model.fit(DF[DPs])
        ET = time()
        ModelDetails["Model"]=model
        ModelDetails["FT"]=1000*(ET-ST)
        StoreModel(ModelDetails, Force)
    else:
        ST = time()
        import pickle
        model = pickle.load(open(ModelDetails["Location"]+"/"+ModelDetails["ModelName"]+".skl", 'rb'))
        ET = time()
        ModelDetails["Model"]=model
        ModelDetails["LT"]=1000*(ET-ST)
        ModelDetails["Model"]=model
        del pickle
    del time
    return ModelDetails
def TrainModel(ModelDetails, DF, DPs, DFName, Force):
    from time import time
    ModelDetails["ModelName"] = ModelDetails["ModelName"]+DFName
    ModelDetails = ModelDetails["Model"](ModelDetails, DF, DPs, Force)
    return ModelDetails
def TestModel(ModelDetails, DF, DPs):
    from time import time
    if (ModelDetails["Type"]=="TF"):
        import tensorflow as tf
        TrainX, TrainY = Convert2TimeSeries(DF, DPs, ModelDetails["TS"], ModelDetails["AE"])
        StartTime = time()
        model = tf.keras.models.load_model(ModelDetails["Location"]+"/"+ModelDetails["ModelName"])
        EndTime = time()
        LT=1000*(EndTime - StartTime)
        del tf
    elif(ModelDetails["Type"]=="SK"):
        import pickle
        StartTime = time()
        model = model = pickle.load(open(ModelDetails["Location"]+"/"+ModelDetails["ModelName"]+".skl", 'rb'))
        EndTime = time()
        LT=1000*(EndTime - StartTime)
        TrainX = DF[DPs]
        del pickle
    else:
        print ("Wrong Model Type, please correct and try again")
        return False
    StartTime = time()
    PredictedValues = model.predict(TrainX)
    EndTime = time()
    PT = 1000*(EndTime-StartTime)
    #print (LT, PT, PredictedValues.reshape(PredictedValues.shape[0]), order='F')
    print (LT, PT, PredictedValues.shape)
    return PredictedValues
def StoreModel(ModelDetails, Force):
    MLModel=ModelDetails["Model"]
    Type = ModelDetails["Type"]
    ModelDirectory=CreateDirectory("", ModelDetails["Location"])
    ModelName = ModelDetails["ModelName"]
    if (Type=="TF"):
        import tensorflow as tf
        if (NeedThisModel(ModelDetails, "TF") or Force):
            MLModel.save(ModelDirectory+ModelName, save_format="tf")
            Con = tf.lite.TFLiteConverter.from_keras_model(MLModel)
            TFLite = Con.convert()
            open(ModelDirectory+ModelName+".tfl", "wb").write(TFLite)
            del tf
            return True
        else:
            print ("Model already exists, try using \"Force=True\"")
            return False
    elif(Type=="SK"):
        import pickle
        if (NeedThisModel(ModelDetails, "SK") or Force):
            pickle.dump(MLModel, open(ModelDirectory+ModelName+".skl", 'wb'))
            del pickle
            return True
        else:
            print ("Model already exists, try using \"Force=True\"")
            return True
    else:
        return False
def SaveTFModel(InputModel, Base, DevType, DPs):
  #TensorFlow Model Directory
  TFDirectory = BaseFolder+DevType+"_"+'-'.join(DPs)
  #Save TF Model
  InputModel.save(TFDirectory, save_format="tf")
  #Convert TF Model to TFLite
  converter = tf.lite.TFLiteConverter.from_saved_model(TFDirectory)
  tflite_model = converter.convert()
  TFL_FILE = MODEL_DIR+".tfl"
  TFM_FILE = MODEL_DIR+".h"
  # Convert and Upload TFL and TFM Models
  open(TFL_FILE, "wb").write(tflite_model)
  #Convert TFLite to TFMicro
  #!xxd -i {TFL_FILE} > {TFM_FILE}
  # Update variable names
  #REPLACE_TEXT = TFL_FILE.replace('/', '_').replace('.', '_')
  #!sed -i 's/'{REPLACE_TEXT}'/g_model/g' {TFM_FILE}
  return "Models saved at "+TFDirectory