import os
import pandas as pd
from datetime import datetime
import pickle
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from scipy import stats
import plotly.express as px
import plotly.graph_objects as go
from time import time
from itertools import combinations
from sklearn.svm import OneClassSVM
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import IsolationForest
import uuid
def StatsTS(DF, TS, fx):
    ar = []
    for i in range (DF.shape[0] - TS):
        m = DF.iloc[i:i+TS]
        m=fx(m.to_numpy())
        ar.append(m)
    for y in range(TS):
        ar.append(m)
    return np.array(ar)
def AnoMLTest():
    print ("AnoML verion 0.02")
def Convert2TimeSeriesWithNumPy(DF, DPs, TS, AE=False):
    Series, Value = [], []
    ST=time()
    DF[DPs] = DF[DPs].fillna(0)
    NPDF = DF[DPs].to_numpy()
    for i in range(len(DF) - TS):
        Series.append(NPDF[i:i+TS])
        if (AE):
            Value.append(NPDF[i:i+TS])
        else:
            Value.append(NPDF[i + TS])
    #Last values (len - TS) will be static
    for j in range(TS):
        Series.append(Series[-1])
        Value.append(Value[-1])
    ET=time()
    TT = 1000*(ET-ST)
    print ("Time consumed in TS conversion", TT, "for ", DF[DPs].shape)
    ST = time()
    Series = np.array(Series)
    Value = np.array(Value)
    ET = time()
    CT = 1000*(ET-ST)
    print ("Converting to NP took", CT)
    return Series, Value, TT
def Convert2TimeSeries(DF, DPs, TS, AE=False):
    Series, Value = [], []
    ST=time()
    DF[DPs] = DF[DPs].fillna(0)
    for i in range(len(DF) - TS):
        Series.append(DF[DPs].iloc[i:i+TS])
        if (AE):
            Value.append(DF[DPs].iloc[i:i+TS])
        else:
            Value.append(DF[DPs].iloc[i + TS])
    #Last values (len - TS) will be static
    for j in range(TS):
        Series.append(Series[-1])
        Value.append(Value[-1])
    ET=time()
    TT = 1000*(ET-ST)
    print ("Time consumed in TS conversion", TT, "for ", DF[DPs].shape)
    ST = time()
    Series = np.array(Series)
    Value = np.array(Value)
    ET = time()
    CT = 1000*(ET-ST)
    print ("Converting to NP took", CT)
    return Series, Value, TT
#Extract Data from all files
def MultiDeviceDataExtractor(FileList, DeviceTypes, SensorColumn):
  DF={}
  for HeaderNumber in range(len(DeviceTypes)):
      DataPoints = DeviceTypes[HeaderNumber].replace("\n", "").split(",")
      DataPoints = ' '.join(DataPoints).split()
      Device = DataPoints[SensorColumn]
      Readings = DataPoints[9:-1]
      Location = DataPoints[-1:]
      columns = [x for x in range(len(DataPoints))]
      temp_df = []
      for ND in FileList:
          tdf=pd.read_csv(ND, header=None, usecols=columns, names=DataPoints)
          temp_df.append(tdf)
      temp_df = pd.concat(temp_df, axis=0, ignore_index=True)
      cond=temp_df[DataPoints[7]]==DataPoints[7]
      DF[Device] = temp_df[cond].copy()
      DF[Device] = DF[Device].reset_index()
      DF[Device]["datetime"] = pd.to_datetime(DF[Device]["datetime"], format="%Y%m%d%H%M", errors="ignore")
      del temp_df
      del cond
      # DF[Device].office_status = [office_status_codes[item] for item in DF[Device].office_status]
      # DF[Device].day = [day_codes[item] for item in DF[Device].day]
      for R in Readings:
          DF[Device][R] = pd.to_numeric(DF[Device][R])
  return DF
def GetDeviceData(DF, DT):
  return DF[DT]
def InvestigateDF(DF):
    Count = DF.isnull().count()
    Sum = DF.isnull().sum()
    Percentage = (Sum/Count*100)
    DataTypes = [str(DF[x].dtype) for x in DF.columns]
    return(np.transpose(pd.DataFrame({'Total':Sum, 'Precent':Percentage, 'Types':DataTypes})))
def CreateDirectory(P, Name):
    path = os.path.join(P, Name)
    try:
        a = os.mkdir(path)
        print ("Directory "+path+" created.")
        del a
    except:
        print ("Directory "+path+" already exists.")
    return path+"/"
def NeedThisModel(ModelDetails, Type):
    # NeedThis Model requires Location, ModelName in a dictionary object. Return True if Model doesn't exists or False if exists.
    # Second parameter is ModelType i.e. TF = TensorFlow or SK = SKLearn
    ModelPath = ModelDetails["Location"]+"/"+ModelDetails["ModelName"]
    TFLite = os.path.isfile(ModelPath+".tfl")
    SKLearn = os.path.isfile(ModelPath+".skl")
    TFMicro = os.path.isfile(ModelPath+".h")
    TFModel = os.path.isdir(ModelPath)
    if (Type=="TF" and TFModel):
        Exists = True
    elif(Type=="TFM" and TFMicro):
        Exists = True
    elif(Type=="TFL" and TFLite):
        Exists = True
    elif (Type=="SK" and SKLearn):
        Exists = True
    else:
        Exists = False
    if (Exists):
        return False
    else:
        return True
#Visualize Device Data
def VisualizeDeviceData(DF, DPs, Title="Train", Location=""):
  #import plotly.io.write_image
  fig = go.Figure()
  for DP in DPs:
    fig.add_trace(go.Scatter(x=DF["datetime"], y=DF[DP], mode='lines', name=DP))
  Title = Title+" "+"-".join(DPs)
  fig.update_layout(
  xaxis_title="Timeline",
  yaxis_title=Title)
  #fig.show()
  fig.write_html(Location+Title+".html")
  fig.data = []
def DPSubsets(DPs, MaxPoints=0, MinPoints=0):
    if (len(DPs)==1):
        #print ("List has only 1 DataPoint, No further subsets possible")
        return DPs
    ST = time()
    SubSet=[]
    if (MaxPoints==0):
        MaxPoints = len(DPs)
    for DP in range(MinPoints, MaxPoints+1):
        for ss in combinations(DPs, DP):
            if (len(ss)>0):
                SubSet.append(ss)
    return SubSet
def DataReducer(DF, DPs, Type):
    if (len(DPs)<=1):
        print ("Atleast two DataPoints should be provided for reduction")
        return False
    ST = time()
    t=[]
    if (Type=="Average"):
        fx=np.average
    elif (Type=="SD"):
        fx=np.std
    elif (Type=="Skew"):
        fx=stats.skew
    elif (Type=="Kurtosis"):
        fx=stats.kurtosis    
    elif (Type=="MAD"):
        try:
            fx=stats.median_abs_deviation
        except:
            fx = stats.median_absolute_deviation
    else:
        print ("Please choose correct reduction technique: Average, SD, Skew, Kurtosis, MAD")
        return False
    for row in DF[DPs].to_numpy():
        row = np.nan_to_num(row)
        t.append(fx(row))
    t = pd.DataFrame(t,columns=['value'])
    ET = time()
    ProcessingTime = (ET-ST)*1000
    print ("Processing Time for "+Type+" Reduction of dataframe of shape: ", DF.shape, " = ", ProcessingTime)
    return t
def DataScaler(DF, DPs, Type="Standard"):
    ST = time()
    DF[DPs] = DF[DPs].fillna(0)
    if (Type=="Standard"):
        
        R = pd.DataFrame(StandardScaler().fit_transform(pd.DataFrame(DF[DPs])),columns=[DPs])
    elif(Type=="MinMax"):       
        R = pd.DataFrame(MinMaxScaler().fit_transform(pd.DataFrame(DF[DPs])),columns=[DPs])
    ET = time()
    ProcessingTime = (ET-ST)*1000
    print ("Processing Time for "+Type+" Scaling of dataframe of shape: ", DF.shape, " = ", ProcessingTime)
    return R
def DataCleanerNormalizer(DF, DateColumn, DataMapping='auto', LabelColumn=False):
    # This function will clean DataFrame from null values, and auto/manual map strings with numbers
    ST = time()
    # Getting all columns in the received DataFrame and convert into list to make it iterable
    Readings = DF.columns
    Readings = Readings.to_list()
    Readings.remove(DateColumn)
    # If lable column was configured, it will be removed from the process. 
    if (LabelColumn!=False):
        Readings.remove(LabelColumn)
    # Get all columns' datatypes to device either clean or normalize
    DataTypes = DF[Readings].dtypes
    # This Dictionary object will hold all mappings' information for later use.
    N = {}
    for i in range(len(Readings)):
        a = Readings[i]
        if (DataTypes[i] == object):
            if (DataMapping=='auto'):
                # This is normal dataset and has not been normalized yet
                U = DF[a].unique()
                ids = [x for x in range(len(U))]
                dic = dict(zip(U, ids))
                N[a]=dic
                DF[a] = DF[a].map(dic)
            else:
                # DataMapping has the mapping information, possible normal dataset has been normalized
                # print (a, DataMapping[a])
                DF[a] = DF[a].map(DataMapping[a])
                N[a] = DataMapping[a]
        else:
            # Convert all columns to numeric format
            DF[a] = pd.to_numeric(DF[a])
            # fill 0 where null values in DataFrame
            DF[a].fillna(0)
    # Convert Data/Time Column with datatime format
    DF[DateColumn] = pd.to_datetime(DF[DateColumn], errors="ignore")
    ET = time()
    ProcessingTime = (ET-ST)*1000
    print ("Processing Time for cleaning and normalization of dataframe of shape: ", DF.shape, " = ", ProcessingTime)
    if (N):
      pass
    else:
      print ("There is no columns to normalize, make sure you are not running this function on pre-normalized dataset.")
    # Function will return Cleaned and Normalized DataFrame, DataPoints, Dictionary Object for Mapping (for reversing the Normalization), 
    # print ()
    return DF, Readings, N
def MergeDeviceData(DF, Devices, DPs):
  M = pd.DataFrame()
  if (DF[Devices[0]].shape[0] >= DF[Devices[1]].shape[0]):
    DF[Devices[0]] = DF[Devices[0]][:DF[Devices[1]].shape[0]]
  elif (DF[Devices[0]].shape[0] < DF[Devices[1]].shape[0]):
    DF[Devices[1]] = DF[Devices[1]][:DF[Devices[0]].shape[0]]
#   print  (DF[Devices[0]].shape[0], DF[Devices[1]].shape[0])
  M["datetime"] = DF[Devices[0]]["datetime"]
  DFName = ""
  for i in range (len(Devices)):
    DFName = DFName+Devices[i]+"_"
    for DP in DPs[i]:
      M[Devices[i]+"_"+DP] = DF[Devices[i]][DP]
      DFName = DFName+DP+"-"
    DFName = DFName+"__"
  DFName = DFName.rstrip("_-")
  return M, DFName
  #Double underscore to sperate Devices, single underscore to seperate devices and sensors and dash to seperate sensors

def CNN1D_Model(ModelDetails, DF, DPs):
    # CNN1D_Model requires keys: batch_size, epochs, units, TS, AE, (Location, ModelName for NeedThisModel function) in dictionary object.
    # It also requires DataFrame as second parameter and DataPoints as third parameter, forth parameter can be True or False
    # True to Force generation of new model and to be written on disk, False for skiping if model already exists.
  TrainX = DF[0]
  TrainY = DF[1]
  if (tf.__version__ != "2.1.0"):
    print ("This function requires tensorflow version 2.1.1, please install required version and retry")
    return ModelDetails, False
  if (NeedThisModel(ModelDetails, "TF") or ModelDetails["Force"]):
    # TrainX, TrainY = Convert2TimeSeries(DF, DPs, ModelDetails["TS"], ModelDetails["AE"])
    early_stop = tf.keras.callbacks.EarlyStopping(
            monitor='loss', min_delta=1e-2, patience=3, verbose=0, mode='auto',
            baseline=None, restore_best_weights=True)
    V = len(DPs)
    StartTime=time()
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Conv1D(filters=ModelDetails["filters"], kernel_size=ModelDetails["kernel_size"], padding='same', activation='relu', input_shape=(ModelDetails["TS"], V)))
    model.add(tf.keras.layers.GlobalMaxPool1D())
    model.add(tf.keras.layers.Dense(units=V, activation='linear'))
    model.compile(optimizer='adam', loss='mean_squared_error')
    history = model.fit(TrainX, TrainY, batch_size=ModelDetails["batch_size"], epochs=ModelDetails["epochs"], callbacks=[early_stop], verbose=0)
    EndTime = time()
    ModelDetails["TT"] = 1000*(EndTime-StartTime)
    StoreModel(ModelDetails, model, ModelDetails["Force"])
    return ModelDetails, model
  else:
      return ModelDetails, False
def RNN_Model(ModelDetails, DF, DPs):
    # RNN_Model requires keys: batch_size, epochs, units, TS, AE, (Location, ModelName for NeedThisModel function) in dictionary object.
    # It also requires DataFrame as second parameter and DataPoints as third parameter, forth parameter can be True or False
    # True to Force generation of new model and to be written on disk, False for skiping if model already exists.
    TrainX = DF[0]
    TrainY = DF[1]
    if (ModelDetails["batch_size"]>1 and len(DPs)>1):
        pass
#         print ("Multivariate data should have a batch size of 1 for this model, changing automatically")
#         ModelDetails["batch_size"]=1
    if (NeedThisModel(ModelDetails, "TF") or ModelDetails["Force"]):
#         TrainX, TrainY = Convert2TimeSeries(DF, DPs, ModelDetails["TS"], ModelDetails["AE"])
        from tensorflow.keras.layers import LSTM, InputLayer, RepeatVector, Dropout, TimeDistributed, Dense
        RNN_Layer=LSTM
        Variants=len(DPs)
        early_stop = tf.keras.callbacks.EarlyStopping(monitor='loss', min_delta=1e-2, patience=3,
                                   verbose=0, mode='auto', baseline=None, restore_best_weights=True)
        StartTime=time()
        model = tf.keras.Sequential()
        #model.add(InputLayer(input_shape=(TrainX.shape[1], TrainX.shape[2]), name='Input_Layer_1'))
        model.add(RNN_Layer(ModelDetails["units"], input_shape=(TrainX.shape[1], TrainX.shape[2]), name="rnn_layer_2"))
        model.add(Dropout(rate=0.2, name="dropout_layer_3"))
        model.add(RepeatVector(n=TrainX.shape[2], name="repeatvector_layer_4"))
        model.add(RNN_Layer(ModelDetails["units"], return_sequences=True, name="rnn_layer_5"))
        model.add(Dropout(rate=0.2, name="dropout_layer_6"))
        model.add(TimeDistributed(Dense(units=TrainX.shape[2], name="timedistributed_layer_7")))
        model.add(Dense(units=1, name="dense_layer_8"))
        model.compile(loss='mae', optimizer='adam')
        model.build()
#         model.summary()
        history = model.fit(x=TrainX, y=TrainY, epochs=ModelDetails["epochs"],
                            batch_size=ModelDetails["batch_size"], callbacks=[early_stop], verbose=1)
        EndTime = time()
        ModelDetails["TT"] = 1000*(EndTime-StartTime)
        print ("Time consumed in model training ", ModelDetails["TT"], " for data shape: ", TrainX.shape)
        StoreModel(ModelDetails, model, ModelDetails["Force"])
        return ModelDetails, model
    else:
        return ModelDetails, False
def OCSVM_Model(ModelDetails, DF, DPs):
    if (NeedThisModel(ModelDetails, "SK") or ModelDetails["Force"]):
        TrainX = DF[0]
        ST = time()
        model = OneClassSVM(nu=ModelDetails["nu"], kernel=ModelDetails["kernel"], gamma=ModelDetails["gamma"])
        model.fit(TrainX)
        ET = time()
        ModelDetails["TT"]=1000*(ET-ST)
        StoreModel(ModelDetails, model, ModelDetails["Force"])
        return ModelDetails, model
def IF_Model(ModelDetails, DF, DPs):
    TrainX = DF[0]
    if (NeedThisModel(ModelDetails, "SK") or ModelDetails["Force"]):
        ST = time()
        model = IsolationForest(contamination=ModelDetails["contaimination"])
        model.fit(TrainX)
        ET = time()
        ModelDetails["TT"]=1000*(ET-ST)
        StoreModel(ModelDetails, model, ModelDetails["Force"])
        return ModelDetails, model
def TrainModel(ModelDetails, DF, DPs, DFName, Force):
    ModelDetails["ModelName"] = ModelDetails["ModelName"]+DFName
    ModelDetails = ModelDetails["Model"](ModelDetails, DF, DPs, Force)
    return ModelDetails
def TestModel(ModelDetails, DF, DPs):
    if (ModelDetails["Type"]=="TF"):
        TrainX, TrainY = Convert2TimeSeries(DF, DPs, ModelDetails["TS"], ModelDetails["AE"])
        StartTime = time()
        model = tf.keras.models.load_model(ModelDetails["Location"]+"/"+ModelDetails["ModelName"])
        EndTime = time()
        LT=1000*(EndTime - StartTime)
    elif(ModelDetails["Type"]=="SK"):
        StartTime = time()
        model = model = pickle.load(open(ModelDetails["Location"]+"/"+ModelDetails["ModelName"]+".skl", 'rb'))
        EndTime = time()
        LT=1000*(EndTime - StartTime)
        TrainX = DF[DPs]
    else:
        print ("Wrong Model Type, please correct and try again")
        return False
    StartTime = time()
    PredictedValues = model.predict(TrainX)
    EndTime = time()
    PT = 1000*(EndTime-StartTime)
    #print (LT, PT, PredictedValues.reshape(PredictedValues.shape[0]), order='F')
    print (LT, PT, PredictedValues.shape)
    return PredictedValues
def StoreModel(ModelDetails, InputModel, Force):
    MLModel=InputModel
    Type = ModelDetails["Type"]
    ModelDirectory=CreateDirectory("", ModelDetails["Location"])
    ModelName = ModelDetails["ModelName"]
    if (Type=="TF"):
        if (NeedThisModel(ModelDetails, "TF") or Force):
            MLModel.save(ModelDirectory+ModelName, save_format="tf")
            Con = tf.lite.TFLiteConverter.from_keras_model(MLModel)
            TFLite = Con.convert()
            open(ModelDirectory+ModelName+".tfl", "wb").write(TFLite)
            return True
        else:
            print ("Model already exists, try using \"Force=True\"")
            return False
    elif(Type=="SK"):
        if (NeedThisModel(ModelDetails, "SK") or Force):
            pickle.dump(MLModel, open(ModelDirectory+ModelName+".skl", 'wb'))
            return True
        else:
            print ("Model already exists, try using \"Force=True\"")
            return True
    else:
        return False
# # Old Function *** will be deleted in next release
# def SaveTFModel(InputModel, BaseFolder, DevType, DPs):
#   #TensorFlow Model Directory
#   TFDirectory = BaseFolder+DevType+"_"+'-'.join(DPs)
#   #Save TF Model
#   InputModel.save(TFDirectory, save_format="tf")
#   #Convert TF Model to TFLite
#   converter = tf.lite.TFLiteConverter.from_saved_model(TFDirectory)
#   tflite_model = converter.convert()
#   TFL_FILE = MODEL_DIR+".tfl"
#   TFM_FILE = MODEL_DIR+".h"
#   # Convert and Upload TFL and TFM Models
#   open(TFL_FILE, "wb").write(tflite_model)
#   #Convert TFLite to TFMicro
#   #!xxd -i {TFL_FILE} > {TFM_FILE}
#   # Update variable names
#   #REPLACE_TEXT = TFL_FILE.replace('/', '_').replace('.', '_')
#   #!sed -i 's/'{REPLACE_TEXT}'/g_model/g' {TFM_FILE}
#   return "Models saved at "+TFDirectory
def TFAnalysis(ModelDetails, X, Y, P, DF, DPs, DFType="Normal"):
    Threshold_Level=8
    TIME_STEPS=X[1]
    Y = np.reshape(Y, P.shape, order='C')
    # Calculate Loss for each row
    MAELoss = np.mean(np.abs(P - Y), axis=1)
    #Calculate Threshold (Mean of MAELoss) + ((Standard Deviation of MAELoss) * SDThreshold) where SDThreshold is configurable
    Threshold = np.mean(MAELoss) + (Threshold_Level*np.std(MAELoss))
    # Adding Loss value for each row in DataFrame
    DF["Loss"]=MAELoss[:-TIME_STEPS]
#     DF["Loss"]=MAELoss
    # Adding recommended Threshold value in DataFrame
    DF["Threshold"]=Threshold
    # Adding Anomaly Column in DataFrame setting True = Anomaly or False = Not Anomaly if Loss > Threshold
    DF["Anomaly"]=DF["Loss"] > DF["Threshold"]
    # Cols will hold neccesary columns for visualization of Anomaly Graph
    Cols = ["datetime", "Loss", "Anomaly"] + DPs
    # Create new dataset which only hold anomlous rows
    AnomaliesDF = DF[DF["Anomaly"]==True][Cols]
    del Cols
    if(DFType=="Normal"):
        ModelDetails["Loss_Mean_"+DFType]= np.mean(MAELoss)
        ModelDetails["Threshold_Train_"+DFType] = Threshold
        ModelDetails["Data_Shape_"+DFType] = X
        ModelDetails["Anomalies_Train_"+DFType] = AnomaliesDF.shape[0]
        return ModelDetails, AnomaliesDF, False
    elif (DFType=="Anomaly"):
        ModelDetails["Loss_Mean_"+DFType]= np.mean(MAELoss)
        ModelDetails["Threshold_Test_"+DFType] = Threshold
        ModelDetails["Data_Shape_"+DFType] = X
        ModelDetails["Anomalies_Test_"+DFType] = AnomaliesDF.shape[0]
        TrainThreshold = ModelDetails["Threshold_Train_Normal"]
        DF["TrainThreshold"]=TrainThreshold
        # There will be two anomalies categories, first by using Threshold of Train Data and second by using Threshold of Test Data
        DF["AnomalyTrainThreshold"]=DF["Loss"] > DF["TrainThreshold"]
        Cols = ["datetime", "Loss", "AnomalyTrainThreshold"] + DPs
        AnomaliesDF_Train = DF[DF["AnomalyTrainThreshold"]==True][Cols]
        ModelDetails["Anomalies_Train_Anomaly"] = AnomaliesDF_Train.shape[0]
        del Cols
        return ModelDetails, AnomaliesDF, AnomaliesDF_Train
def SKAnalysis(ModelDetails, SKModel, X, Y, DF, DPs, DFType="Normal"):
    P = SKModel.decision_function(X)
    DF["Anomaly"]=SKModel.predict(X)
    MAELoss = np.mean(np.abs(P - Y), axis=1)
    Cols = ["datetime", "Loss", "Anomaly"] + DPs
    AnomaliesDF = DF[DF["Anomaly"]==-1][Cols]
    ModelDetails["Loss_Mean_"+DFType]= MAELoss
    ModelDetails["Threshold_Train_"+DFType] = 0
    ModelDetails["Data_Shape_"+DFType] = X.shape
    ModelDetails["Anomalies_Train_"+DFType] = AnomaliesDF.shape[0]
    return ModelDetails, AnomaliesDF
###############Solving Sync Issues###############
def FillDF(Index, MainDF, NumberOfRows, Frequency):
    #Seperate MainDF into two DFs
    A = MainDF.iloc[:Index, ]
    B = MainDF.iloc[Index:, ]
    C = pd.DataFrame()
    for i in range(NumberOfRows):
        entry = MainDF.loc[Index-1].copy()
        entry['datetime']=entry['datetime'] + pd.to_timedelta(Frequency*(i+1), unit='s')
        A=A.append([entry])
    B = B.iloc[1: , :]
    B.drop_duplicates(subset=['datetime'], keep='first', inplace=True, ignore_index=False)
    A.drop_duplicates(subset=['datetime'], keep='first', inplace=True, ignore_index=False)
    MainDF = A.append(B).reset_index(drop = True)
    return MainDF
def DateDifference(Date1, Date2):
    Date1 = datetime.strptime(str(Date1), "%Y-%m-%d %H:%M:%S")
    Date2 = datetime.strptime(str(Date2), "%Y-%m-%d %H:%M:%S")
    D = abs((Date2 - Date1).days)
    return D
def TimeDifference(Time1, Time2):
    Time1 = datetime.strptime(str(Time1), "%Y-%m-%d %H:%M:%S")
    Time2 = datetime.strptime(str(Time2), "%Y-%m-%d %H:%M:%S")
    T = abs((Time2 - Time1).total_seconds())
    return T
def RepairDF(DF, Frequency, StartTime, EndTime):
    grouper=DF
    grouper["gap"] = grouper["datetime"].diff().dt.total_seconds() > Frequency
    grouper["gapinsecs"]=grouper["datetime"].diff().dt.total_seconds()
    AllSorted=False
    index=0
#     print ("Pre Gap filling", grouper.shape)
    t=0
    while not AllSorted:
        if index==grouper.index[-1]:
            AllSorted=True
            continue
        try:
            if grouper.iloc[index]["gap"]:
                newrows=0
                t1=grouper.iloc[index-1]["datetime"]
                t2=grouper.iloc[index]["datetime"]
                Ds = DateDifference(t1, t2)
                newrows = int((pd.Timedelta(t2 - t1).seconds) / Frequency)
                newrows = int(((Ds*24*60*60)/Frequency)+newrows)
                t=t+newrows
                grouper = FillDF(index, grouper, newrows, Frequency)
                index+=newrows+1
            else:
                index+=1
        except:
            print (AllSorted, t, index)
            break
#     print ("Post Gap filling", grouper.shape)
#     print (t, "total new rows addded")
    grouper["new_gap"] = grouper["datetime"].diff().dt.seconds > Frequency
    StartDateTime = grouper.iloc[0]["datetime"]
    EndDateTime = grouper.iloc[-1]["datetime"]
#     print (StartDateTime ,EndDateTime)
    Days = DateDifference(EndDateTime, StartDateTime)
    #     print ("Days", Days, "Hours", Days*24, "Minutes", Days*24*60, "Possible Rows", (Days*24*60*60)/300)
    #     print ("Gap more than 5 minutes", grouper["new_gap"].value_counts())
#     grouper.to_csv("/home/c.c1879106/temp/key_filled.csv")
#     VisualizeDeviceData(grouper, [grouper.columns[10]])
    RowsDiff = ((Days*24*60*60)/Frequency)-grouper.shape[0]
#     print ("Difference: ", RowsDiff, ", ", ((RowsDiff*Frequency)/60)/60, "hours data missing")
    return grouper
# # Let's test now
# DFT = Normal_df['sense-hat'].copy()
# DFT = RepairDF(DFT, 300, "", "")
# DFT.head()
# ###############Solving Sync Issues###############